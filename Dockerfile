FROM docker.elastic.co/logstash/logstash:7.6.2

RUN  /usr/share/logstash/bin/logstash-plugin update logstash-filter-dns
COPY logstash/synlite_suricata /usr/share/logstash/config/synlite_suricata

RUN echo -e '- pipeline.id: synlite_suricata\n  path.config: "/usr/share/logstash/config/synlite_suricata/conf.d/*.conf"' > /usr/share/logstash/config/pipelines.yml

ENV SYNLITE_SURICATA_DICT_PATH=/usr/share/logstash/config/synlite_suricata/dictionaries
ENV SYNLITE_SURICATA_TEMPLATE_PATH=/usr/share/logstash/config/synlite_suricata/templates
ENV SYNLITE_SURICATA_GEOIP_DB_PATH=/usr/share/logstash/config/synlite_suricata/geoipdbs
ENV SYNLITE_SURICATA_GEOIP_CACHE_SIZE=8192
ENV SYNLITE_SURICATA_GEOIP_LOOKUP=true
ENV SYNLITE_SURICATA_ASN_LOOKUP=true
ENV SYNLITE_SURICATA_CLEANUP_SIGS=false

# Name resolution option
ENV SYNLITE_SURICATA_RESOLVE_IP2HOST=true
ENV SYNLITE_SURICATA_NAMESERVER=192.168.0.65
ENV SYNLITE_SURICATA_DNS_HIT_CACHE_SIZE=25000
ENV SYNLITE_SURICATA_DNS_HIT_CACHE_TTL=900
ENV SYNLITE_SURICATA_DNS_FAILED_CACHE_SIZE=75000
ENV SYNLITE_SURICATA_DNS_FAILED_CACHE_TTL=3600

# Elasticsearch connection settings
ENV SYNLITE_SURICATA_ES_HOST=192.168.0.84
ENV SYNLITE_SURICATA_ES_USER=""
ENV SYNLITE_SURICATA_ES_PASSWD=""

# Beats input
ENV SYNLITE_SURICATA_BEATS_HOST=0.0.0.0
ENV SYNLITE_SURICATA_BEATS_PORT=5044
